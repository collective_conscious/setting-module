const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');


mix.setPublicPath('../../public/app').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/settings.js', 'js/setting/settings.js');

if (mix.inProduction()) {
    mix.version();
}