<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/30/2019
 * Time: 12:17 PM
 */

namespace Modules\Setting\Actions;


use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BackupDatabaseToGoogleDrive
{
    public function execute() {
        $database = env('DB_DATABASE', 'forge');
        $user = env('DB_USERNAME', 'forge');
        $pass = env('DB_PASSWORD', '');
        $host = env('DB_HOST', '127.0.0.1');

        $fileName = $database . '_' . Carbon::now()->format('YmdHi') . '_dump.sql';
        $path = 'storage/exports/backup/'. $fileName;
        $dir = public_path($path);

        try {
            exec("mysqldump --user={$user} --password={$pass} --host={$host} {$database} --result-file={$dir} 2>&1", $output);

            $fileContent = File::get($dir);

            $driver = Storage::disk('google');

            if($driver) {
                $result = $driver->put($fileName, $fileContent);

                File::delete($dir);

                if ($result) {
                    return [
                        'code' => 200,
                        'path' => Storage::disk('google')->url($fileName),
                        'filename' => $fileName
                    ];
                } else {
                    return response()->json([
                        'success' => false,
                        'code' => 400,
                        'message' => 'Google Drive Not Working'
                    ]);
                }
            }
            else {
                return [
                    'code' => 200,
                    'path' => $path,
                    'filename' => $fileName,
                    'message' => 'File has been saved locally'
                ];
            }

        }
        catch (\Exception $exception) {
            return [
                'code' => 400,
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace()
            ];
        }
    }
}