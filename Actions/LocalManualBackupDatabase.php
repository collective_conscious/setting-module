<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/30/2019
 * Time: 12:17 PM
 */

namespace Modules\Setting\Actions;


use Carbon\Carbon;

class LocalManualBackupDatabase
{
    public function execute() {
        $database = env('DB_DATABASE', 'forge');
        $user = env('DB_USERNAME', 'forge');
        $pass = env('DB_PASSWORD', '');
        $host = env('DB_HOST', '127.0.0.1');

        $fileName = $database . '_' . Carbon::now()->format('YmdHi') . '_dump.sql';
        $path = 'storage/exports/backup/'. $fileName;
        $dir = public_path($path);

        exec("mysqldump --user={$user} --password={$pass} --host={$host} {$database} --result-file={$dir} 2>&1", $output);

        return [
            'path' => $path,
            'filename' => $fileName
        ];
    }
}