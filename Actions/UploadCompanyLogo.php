<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/28/2019
 * Time: 3:25 PM
 */

namespace Modules\Setting\Actions;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Setting\Repositories\SettingRepository;

class UploadCompanyLogo
{
    private $repository;

    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @param $col
     * @param $where
     * @return mixed
     */
    public function execute(array $data, $col, $where) {
        $document = $data[$col];

        $NewFileName = basename($document->getClientOriginalName(), '.' . $document->getClientOriginalExtension()) . '_' . time() . '.' . $document->getClientOriginalExtension();

        Storage::disk('public')->putFileAs($where, $document, $NewFileName);

        $filename = $where.'/'.$NewFileName;

        return $filename;
    }
}