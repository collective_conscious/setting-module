<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/28/2019
 * Time: 3:25 PM
 */

namespace Modules\Setting\Actions;


use Illuminate\Support\Facades\Auth;
use Modules\Setting\Repositories\SettingRepository;

class CreateSetting
{
    private $repository;

    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $data) {
        return $this->repository->create([
            'user_id' => Auth::id(),
            'key' => $data['key'],
            'value' => $data['val'],
            'is_default' => $data['is_default'] ?? 1,
            'options' => $data['options'] ?? null,
        ]);
    }
}