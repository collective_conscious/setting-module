<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/28/2019
 * Time: 3:25 PM
 */

namespace Modules\Setting\Actions;


use Illuminate\Support\Facades\Auth;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Setting\Traits\SettingTrait;

class UpdateOneSetting
{
    use SettingTrait;

    private $repository;
    protected $createSetting;
    protected $uploadCompanyLogo;

    public function __construct(SettingRepository $repository, CreateSetting $createSetting, UploadCompanyLogo $uploadCompanyLogo)
    {
        $this->repository = $repository;
        $this->createSetting = $createSetting;
        $this->uploadCompanyLogo = $uploadCompanyLogo;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute($data, $col, $val) {

        $record = $this->getSettingValueByKey($this->repository, $col);

        $data['key'] = $col;
        $data['val'] = $val;

        if($col == 'company_logo') {
            $data['val'] = $this->uploadCompanyLogo->execute($data, $col, 'images/profile');
        }

        if($record) {
            return $this->repository->update([
                'value' => $data['val'],
            ], $record->id);
        }
        else {
            $this->createSetting->execute($data);
        }
    }
}