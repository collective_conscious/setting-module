<?php

namespace Modules\Setting\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Setting extends Model
{
    /*
     * If Activity Log Module is not added, Please remove LogsActivity and CausesActivity
     * Also remove the protected attributes $logName, $logAttributes, $logOnlyDirty
     */

    /*
     * To add Activity Module, update composer.json
     * add this package
     *
     * collective_conscious/activity-module
     *
     */

    use LogsActivity;  /* Remove in case, No Activity Module */

    protected static $logName = 'setting'; /* Remove in case, No Activity Module */
    protected static $logAttributes = ['*'];/* Remove in case, No Activity Module */
    protected static $logOnlyDirty = true;/* Remove in case, No Activity Module */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'key', 'value', 'options', 'is_default'
    ];
}
