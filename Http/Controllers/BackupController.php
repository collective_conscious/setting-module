<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/30/2019
 * Time: 12:16 PM
 */

namespace Modules\Setting\Http\Controllers;


use App\Http\Controllers\Controller;
use Modules\Setting\Actions\LocalManualBackupDatabase;

class BackupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function manualBackupAndDownload(LocalManualBackupDatabase $action){

        try {
            $result = $action->execute();

            if($result['code'] == 200) {
                return response()->json([
                    'success' => true,
                    'code' => 200,
                    'msg' => 'Data has been successfully backed up and available for download.',
                    'file' => [
                        'url' => $result['path'],
                        'filename' => $result['filename']
                    ]
                ]);
            }
            else {
                return response()->json([
                    'success' => false,
                    'code' => 422,
                    'msg' => $result['message'] ?? 'Some Error',
                    'exception' => $result['trace'] ?? 'Trace yourself'
                ]);
            }
        }
        catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'code' => 422,
                'msg' => $exception->getMessage(),
                'exception' => $exception->getTrace()
            ]);
        }
    }
}