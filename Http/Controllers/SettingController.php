<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Modules\Setting\Actions\UpdateOneSetting;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Setting\Traits\SettingTrait;

class SettingController extends Controller
{
    use SettingTrait;

    public $repository;

    /**
     * Create a new controller instance.
     *
     * @param SettingRepository $repository
     */
    public function __construct(SettingRepository $repository)
    {
        $this->middleware(['auth']);
        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        $companyLogo = $this->getSettingValueByKey($this->repository, 'company_logo')->value ?? null;
        $companyName = $this->getSettingValueByKey($this->repository, 'company_name')->value ?? null;
        $companyAddress = $this->getSettingValueByKey($this->repository, 'company_address')->value ?? null;
        $automaticBackup = $this->getSettingValueByKey($this->repository, 'automatic_backup')->value ?? null;
        $automaticBackupSchedule = $this->getSettingValueByKey($this->repository, 'automatic_backup_schedule')->value ?? null;
        $automaticEmailOutputTo = $this->getSettingValueByKey($this->repository, 'automatic_backup_email_output_to')->value ?? null;
        $googleDriveClientId = $this->getSettingValueByKey($this->repository, 'google_drive_client_id')->value ?? null;
        $googleDriveClientSecret = $this->getSettingValueByKey($this->repository, 'google_drive_client_secret')->value ?? null;
        $googleDriveRefreshToken = $this->getSettingValueByKey($this->repository, 'google_drive_refresh_token')->value ?? null;
        $googleDriveMainFolderId = $this->getSettingValueByKey($this->repository, 'google_drive_main_folder_id')->value ?? null;

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'code' => 200,
                'view' => view('setting::content', compact(
                    'companyLogo',
                    'companyName',
                    'companyAddress',
                    'automaticBackup',
                    'automaticBackupSchedule',
                    'automaticEmailOutputTo',
                    'googleDriveClientId',
                    'googleDriveClientSecret',
                    'googleDriveRefreshToken',
                    'googleDriveMainFolderId'
                ))->render(),
            ]);
        }
        else
            return view('setting::index', compact(
                'companyLogo',
                'companyName',
                'companyAddress',
                'automaticBackup',
                'automaticBackupSchedule',
                'automaticEmailOutputTo',
                'googleDriveClientId',
                'googleDriveClientSecret',
                'googleDriveRefreshToken',
                'googleDriveMainFolderId'
            ));
    }

    /**
     * @param $col
     * @param Request $request
     * @param UpdateOneSetting $action
     * @return \Illuminate\Http\JsonResponse
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function updateOne($col, Request $request, UpdateOneSetting $action){
        if($col) {
            $action->execute($request->all(), $col, $request->get($col));

            Cache::forget('settings:all');

            return response()->json([
                'success' => true,
                'code' => 200,
                'msg' => 'Setting Updated Successfully.',
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Setting cannot be updated.',
            ]);
        }
    }
}
