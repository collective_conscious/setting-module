<?php

namespace Modules\Setting\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Setting\Console\BackupDatabaseCommand;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Setting\Traits\SettingTrait;

class SettingServiceProvider extends ServiceProvider
{
    use SettingTrait;

    protected $settingRepository;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot(SettingRepository $settingRepository)
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        if ($this->runningInConsole()) {
            $this->publishResources();
        }

        $this->settingRepository = $settingRepository;

        view()->composer(['*'], function($view) {

            $companyLogo = $this->getSettingValueByKey($this->settingRepository,'company_logo')->value ?? null;

            $view->with('company_logo', $companyLogo);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(GoogleDriverServiceProvider::class);

        $this->commands([
            BackupDatabaseCommand::class
        ]);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('setting.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'setting'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/setting');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/setting';
        }, \Config::get('view.paths')), [$sourcePath]), 'setting');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/setting');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'setting');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'setting');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Publish the package's js, css files.
     *
     * @return void
     */
    protected function publishResources()
    {
        $stub = __DIR__.'/../Resources/assets/js/';

        $target = public_path('/app/js/setting');

        $this->publishes([$stub => $target], 'resources');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * Determine if we are running in the console.
     *
     * Copied from Laravel's Application class, since we need to support 5.1.
     *
     * @return bool
     */
    protected function runningInConsole()
    {
        return php_sapi_name() == 'cli' || php_sapi_name() == 'phpdbg';
    }
}
