<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/30/2019
 * Time: 2:40 PM
 */

namespace Modules\Setting\Providers;


use Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Setting\Traits\SettingTrait;

class GoogleDriverServiceProvider extends ServiceProvider
{
    use SettingTrait;

    /**
     * Bootstrap the application services.
     *
     * @param SettingRepository $settingRepository
     * @return void
     */
    public function boot(SettingRepository $settingRepository)
    {
        Storage::extend('google', function($app, $config) use ($settingRepository) {

            $googleDriveClientId = $this->getSettingValueByKey($settingRepository, 'google_drive_client_id')->value ?? null;
            $googleDriveClientSecret = $this->getSettingValueByKey($settingRepository, 'google_drive_client_secret')->value ?? null;
            $googleDriveRefreshToken = $this->getSettingValueByKey($settingRepository, 'google_drive_refresh_token')->value ?? null;
            $googleDriveMainFolderId = $this->getSettingValueByKey($settingRepository, 'google_drive_main_folder_id')->value ?? null;

            if($googleDriveClientId && $googleDriveClientSecret && $googleDriveRefreshToken && $googleDriveRefreshToken) {
                $client = new \Google_Client();

                $client->setClientId($googleDriveClientId);
                $client->setClientSecret($googleDriveClientSecret);
                $client->refreshToken($googleDriveRefreshToken);

                $service = new \Google_Service_Drive($client);

                $options = [];

                if (isset($config['teamDriveId'])) {
                    $options['teamDriveId'] = $config['teamDriveId'];
                }

                $adapter = new GoogleDriveAdapter($service, $googleDriveMainFolderId, $options);

                return new \League\Flysystem\Filesystem($adapter);
            }

            return null;
        });
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}