<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
     * Starting Routes for SettingController
     */

Route::group(['middleware' => ['can:View_Setting']], function () {
    Route::get('settings', 'SettingController@index')->name('settings.index');
});

Route::group(['middleware' => ['can:Add_Setting']], function () {
    Route::get('settings/create', 'SettingController@create')->name('settings.create');
    Route::post('settings', 'SettingController@store')->name('settings.store');
});

Route::group(['middleware' => ['can:Edit_Setting']], function () {
    Route::get('settings/{id}/edit', 'SettingController@edit')->name('settings.edit');
    Route::patch('settings/{id}', 'SettingController@update')->name('settings.update');

    Route::post('settings/{col}/update_one', 'SettingController@updateOne')->name('settings.update_one');
    Route::get('settings/manual/backupAndDownload', 'BackupController@manualBackupAndDownload');
});

Route::group(['middleware' => ['can:Delete_Setting']], function () {
    Route::delete('settings', 'SettingController@destroy')->name('settings.destroy');
});

/*
 * Ending Routes for SettingController
 */
