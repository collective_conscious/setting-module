<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/28/2019
 * Time: 2:43 PM
 */

namespace Modules\Setting\Repositories\Criteria;


use CollectiveConscious\RepositoryDesignPattern\Contracts\CriteriaInterface;
use CollectiveConscious\RepositoryDesignPattern\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

class SettingByKeyCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('key', '=',$this->request->key);
    }
}