<?php

namespace Modules\Setting\Repositories;

use CollectiveConscious\RepositoryDesignPattern\Repository;
use Modules\Setting\Entities\Setting;

class SettingRepository extends Repository
{
    public function model()
    {
        return Setting::class;
    }
}