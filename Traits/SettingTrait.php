<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/28/2019
 * Time: 2:40 PM
 */

namespace Modules\Setting\Traits;


use Illuminate\Http\Request;
use Modules\Setting\Repositories\Criteria\SettingByKeyCriteria;
use Modules\Setting\Repositories\SettingRepository;

trait SettingTrait
{
    /**
     * @param SettingRepository $settingRepository
     * @param $key
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function getSettingValueByKey(SettingRepository $settingRepository, $key){
        $request = new Request();
        $request->key = $key;

        $criteria = new SettingByKeyCriteria($request);

        return $settingRepository->getByCriteria($criteria)->first();
    }
}