<?php

namespace Modules\Setting\Console;

use Illuminate\Console\Command;
use Modules\Setting\Actions\BackupDatabaseToGoogleDrive;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BackupDatabaseCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'backup:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command backups data and returns the download link.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param BackupDatabaseToGoogleDrive $action
     * @return mixed
     */
    public function handle(BackupDatabaseToGoogleDrive $action)
    {
        $response = $action->execute();

        if($response['code'] == 200) {
            $this->info($response['path']);
        }
        else {
            $this->error($response['message']);
        }
    }
}
