function viewImage(src) {
    imageViewerModal = document.getElementById('imageViewerModal');
    var imageViewerModalImage = document.getElementById('imageViewerModalImage');

    imageViewerModal.style.display = "block";
    imageViewerModalImage.src = src;
}

function updateOneSetting(url, col, val, msg) {

    $('#AjaxMainErrorAlert').addClass('hide');
    $('#AjaxMainErrorAlert').html('');

    swal({
            title: "Are you sure?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Update it!",
            closeOnConfirm: true,
            showLoaderOnConfirm: true
        },
        function(){
            var data = {};

            data[col] = val;

            var request = $.ajax({ url: window.BaseURL+'/'+url, type: 'POST', data: data});

            request
                .done(function(data, textStatus, xhr){
                    if (data.success) {
                        setTimeout(function () {
                            $('#AjaxSuccessAlert').removeClass('hide');
                            $('#AjaxSuccessAlert span').html(data.msg);
                        }, 800);
                    }
                })
                .fail(function(data, textStatus, xhr) {
                    console.log(data);
                    switch(data.status) {
                        case 401:
                            switch (data.responseJSON.code) {
                                case 401:
                                    swal({
                                            title: "Authentication Failed",
                                            text: data.responseJSON.msg,
                                            type: "error",
                                            showCancelButton: true,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Login",
                                            closeOnConfirm: true
                                        },
                                        function(){
                                            window.location.href = window.LoginURL;
                                        });
                                    break;

                                case 402:
                                    swal({
                                        title: "Authorization Failed",
                                        text: "You are not allowed to perform this action.",
                                        type: "error"
                                    });
                                    break;

                                default:
                                    swal({
                                        title: "Something went wrong",
                                        text: "Error has been logged and We are looking in to the issue. Your patience is appreciated.",
                                        type: "error"
                                    });
                                    break;
                            }
                            break;

                        case 422:
                            $('#AjaxMainErrorAlert').removeClass('hide');
                            $('#AjaxMainErrorAlert').html(data.responseJSON.msg);
                            break;

                        default:
                            swal({
                                title: "Something went wrong",
                                text: "Error has been logged and We are looking in to the issue. Your patience is appreciated.",
                                type: "error",
                            });
                            break;

                    }
                });
        });
}

$('.singleInputs').on('focusout', function () {
    if(canUpdateEntries) {

        var thisElement = $(this);
        var url = thisElement.attr('data-url');
        var col = thisElement.attr('data-col');
        var msg = thisElement.attr('data-msg');
        var val = thisElement.val();

        if (val !== '' && val !== null && val !== undefined)
            updateOneSetting(url, col, val, msg);
    }
});

$('.singleSettingCheckboxes').on('change', function () {
    var thisElement = $(this);
    var url = thisElement.attr('data-url');
    var col = thisElement.attr('data-col');
    var msg = thisElement.attr('data-msg');
    var val = (thisElement.prop("checked"))?1:0;

    if(val !== '' && val !== null && val !== undefined)
        updateOneSetting(url, col, val, msg);
});

$('.singleSelects').on('change', function () {
    var thisElement = $(this);
    var url = thisElement.attr('data-url');
    var col = thisElement.attr('data-col');
    var msg = thisElement.attr('data-msg');
    var val = thisElement.val();

    if(val !== '' && val !== null && val !== undefined)
        updateOneSetting(url, col, val, msg);
});

$('#company_logo').fileupload({
    url: companyLogoUploadUrl,
    dataType: 'json',
    done: function (e, data) {
        if(data.result.success) {

            console.log(data);
        }
    },
    fail: function(data, textStatus, xhr) {
        console.log(data);
        $('#progress .progress-bar').css(
            'width',
            0 + '%'
        );

        switch(data.status) {
            case 401:
                switch (data.responseJSON.code) {
                    case 401:
                        $('#InputModal').modal('hide');
                        swal({
                                title: "Authentication Failed",
                                text: data.responseJSON.msg,
                                type: "error",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Login",
                                closeOnConfirm: true
                            },
                            function(){
                                window.location.href = window.LoginURL;
                            });
                        break;

                    case 402:
                        $('#InputModal').modal('hide');
                        swal({
                            title: "Authorization Failed",
                            text: "You are not allowed to perform this action.",
                            type: "error"
                        });
                        break;

                    default:
                        $('#InputModal').modal('hide');
                        swal({
                            title: "Something went wrong",
                            text: "Error has been logged and We are looking in to the issue. Your patience is appreciated.",
                            type: "error"
                        });
                        break;
                }
                break;

            case 422:
                $.each(data.result.Files, function (index, file) {
                    $('#InputModal').find('#files_list').html(file.name + ' (' + file.size + ' KB) &nbsp;&nbsp;&nbsp;<i class="fa fa-close" style="color: red"></i><hr>');
                });

                switch (data.responseJSON.code) {
                    case 400:
                        $('#AjaxErrorAlert').removeClass('hide');
                        $('#AjaxErrorAlert').html(data.responseJSON.msg);
                        break;

                    default:
                        $('#AjaxErrorAlert').removeClass('hide');

                        var errors = '';
                        $.each(data.responseJSON.errors, function ($index, $item) {
                            errors += $item + '<br>';
                        });

                        $('#AjaxErrorAlert').html(errors);
                        break;
                }

                break;

            default:
                $('#InputModal').modal('hide');
                swal({
                    title: "Something went wrong",
                    text: "Error has been logged and We are looking in to the issue. Your patience is appreciated.",
                    type: "error",
                });
                break;

        }
    },
    progressall: function (e, data) {

        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }
}).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');

$('#backupDb').on('click', function () {
    var request = $.ajax({ url: window.BaseURL+'/settings/manual/backupAndDownload', type: 'GET'});

    request
        .done(function(data, textStatus, xhr){
            if (data.success) {
                console.log(data);
                var file = data.file;

                var a = document.createElement("a");
                a.href = file.url;
                a.setAttribute("download", file.filename);
                a.click();
            }
        })
        .fail(function(data, textStatus, xhr) {
            console.log(data);
            switch(data.status) {
                case 401:
                    switch (data.responseJSON.code) {
                        case 401:
                            swal({
                                    title: "Authentication Failed",
                                    text: data.responseJSON.msg,
                                    type: "error",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Login",
                                    closeOnConfirm: true
                                },
                                function(){
                                    window.location.href = window.LoginURL;
                                });
                            break;

                        case 402:
                            swal({
                                title: "Authorization Failed",
                                text: "You are not allowed to perform this action.",
                                type: "error"
                            });
                            break;

                        default:
                            swal({
                                title: "Something went wrong",
                                text: "Error has been logged and We are looking in to the issue. Your patience is appreciated.",
                                type: "error"
                            });
                            break;
                    }
                    break;

                case 422:
                    $('#AjaxMainErrorAlert').removeClass('hide');
                    $('#AjaxMainErrorAlert').html(data.responseJSON.msg);
                    break;

                default:
                    swal({
                        title: "Something went wrong",
                        text: "Error has been logged and We are looking in to the issue. Your patience is appreciated.",
                        type: "error",
                    });
                    break;

            }
        });
});
