@if($user->can('View_Setting'))
    <li class="heading">
        <h3 class="uppercase">Settings</h3>
    </li>
@endif

@if($user->can('View_Setting'))
    <li class="navbar_ajax nav-item {{ (Request::is('settings') ? 'active' : '') }}" id="NavBar_settings">
        <a href="javascript:LoadView('settings', 'get', null, true);" class="nav-link ">
            <i class="fa fa-gears"></i>
            <span class="title">Settings</span>
            <span id="NavBar_arrow_settings" class="navbar_arrow {{ ((Request::is('settings')) ? 'selected' : '') }}"></span>
        </a>
    </li>
@endif