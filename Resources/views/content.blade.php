<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css') }}" rel="stylesheet" type="text/css" />

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ url('home') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>General Settings</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <div class="btn-group btn-group-solid">
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> General Settings
    <small>General Settings are use all over the system.</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->

<script type="text/javascript">
    canUpdateEntries = {{ (Auth::user()->can(['Edit_Setting']))?'true':'false' }};
</script>

<div class="alert alert-success alert-dismissible hide" id="AjaxSuccessAlert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span></span>
</div>

<div class="alert alert-danger alert-dismissible hide" id="AjaxMainErrorAlert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span></span>
</div>

<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"></span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tabGeneralSettings" data-toggle="tab">General Settings</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <!-- Templates TAB -->
                        <div class="tab-pane active" id="tabGeneralSettings">
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class=" fa fa-gear font-green"></i>
                                                <span class="caption-subject font-green bold uppercase">Settings</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-group">
                                                <label class="control-label">Company Name</label>
                                                <input type="text" value="{{ $companyName }}" class="form-control singleInputs" data-url="settings/company_name/update_one" data-col="company_name" data-msg="You want to update company name?" name="company_name" style="resize: none;" placeholder="Enter Name of the Company.">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Company Address</label>
                                                <textarea class="form-control singleInputs" data-url="settings/company_address/update_one" data-col="company_address" data-msg="You want to update company address?" name="company_address" style="resize: none;" rows="5" placeholder="Enter Detailed Address. It will be displayed on voucher and invoices.">{{ $companyAddress }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    @if(Auth::user()->can(['Automatic_Backup']))
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" fa fa-gear font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Automatic Backups are {!! ($automaticBackup)?'<span class="label label-success">On</span>':'<span class="label label-danger">Off</span>' !!}</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="form-group col-md-4">
                                                    <label class="mt-checkbox mt-checkbox-warning mt-checkbox-outline"> Change Status
                                                        <input type="checkbox" {{ ($automaticBackup)?'checked':'' }} value="{{ $automaticBackup }}" name="automatic_backup" class="singleSettingCheckboxes" data-url="settings/automatic_backup/update_one" data-col="automatic_backup" data-msg="You want to update status of automatic backup?" >
                                                        <span></span>
                                                    </label>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label class="control-label">Schedule Backup </label>
                                                    <select class="form-control select2 singleSelects" data-url="settings/automatic_backup_schedule/update_one" data-col="automatic_backup_schedule" data-msg="You want to update backup scheduling?" name="automatic_backup_schedule">
                                                        <option class="daily" {{ ($automaticBackupSchedule == 'Daily')?'selected':'' }}>Daily</option>
                                                        <option class="3 days" {{ ($automaticBackupSchedule == '3 Days')?'selected':'' }}>3 Days</option>
                                                        <option class="weekly" {{ ($automaticBackupSchedule == 'Weekly')?'selected':'' }}>Weekly</option>
                                                    </select>
                                                </div>


                                                <div class="form-group col-md-4">
                                                    <label class="control-label">Email Output To</label>
                                                    <input type="text" value="{{ $automaticEmailOutputTo }}" class="form-control singleInputs" data-url="settings/automatic_backup_email_output_to/update_one" data-col="automatic_backup_email_output_to" data-msg="You want to update google drive api details?" name="automatic_backup_email_output_to" style="resize: none;" placeholder="Enter Google Drive Client ID.">
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="form-group">
                                                    <label class="control-label">Google Drive Client ID</label>
                                                    <input type="text" value="{{ $googleDriveClientId }}" class="form-control singleInputs" data-url="settings/google_drive_client_id/update_one" data-col="google_drive_client_id" data-msg="You want to update google drive api details?" name="google_drive_client_id" style="resize: none;" placeholder="Enter Google Drive Client ID.">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Google Drive Client Secret</label>
                                                    <input type="text" value="{{ $googleDriveClientSecret }}" class="form-control singleInputs" data-url="settings/google_drive_client_secret/update_one" data-col="google_drive_client_secret" data-msg="You want to update google drive api details?" name="google_drive_client_secret" style="resize: none;" placeholder="Enter Google Drive Client Secret.">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Google Drive Refresh Token</label>
                                                    <input type="text" value="{{ $googleDriveRefreshToken }}" class="form-control singleInputs" data-url="settings/google_drive_refresh_token/update_one" data-col="google_drive_refresh_token" data-msg="You want to update google drive api details?" name="google_drive_refresh_token" style="resize: none;" placeholder="Enter Google Drive Client ID.">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Google Drive Main Folder Id</label>
                                                    <input type="text" value="{{ $googleDriveMainFolderId }}" class="form-control singleInputs" data-url="settings/google_drive_main_folder_id/update_one" data-col="google_drive_main_folder_id" data-msg="You want to update google drive api details?" name="google_drive_main_folder_id" style="resize: none;" placeholder="Enter Google Drive Client ID.">
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-sm-3">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class=" fa fa-photo font-green"></i>
                                                <span class="caption-subject font-green bold uppercase">Company Logo</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <form action="{{ url('settings/company_logo/update_one') }}" role="form" method="post" data-load-after="settings" data-load-after-method="get">
                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="{{ ($companyLogo != null)?url('storage/'.$companyLogo):'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' }}" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>

                                                        @if(Auth::user()->can(['Edit_Setting']))
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" required="required" id="company_logo" name="company_logo">
                                                                </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div id="progress" class="progress">
                                                        <div class="progress-bar progress-bar-success"></div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    @if(Auth::user()->can(['Download_Backup']))
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" fa fa-photo font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Backup Database</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="button" id="backupDb" class="btn m-btn--air btn-success btn-lg" style="width: 100%">
                                                            <span>
                                                                <i class="la la-cloud-download"></i>
                                                                <span>Backup Database</span><br>
                                                                <span style="color: white; font-size: 9px;">This only downloads the latest collection<br> of all your data. </span>
                                                            </span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->

@include('layout._modalImageViewer')

<script type="text/javascript">
    companyLogoUploadUrl = "{{ url('settings/company_logo/update_one') }}";
    fileUpload = true;
</script>

<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/js/setting/settings.js?'.time()) }}" type="text/javascript"></script>